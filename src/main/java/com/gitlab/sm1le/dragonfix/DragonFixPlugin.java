package com.gitlab.sm1le.dragonfix;

import com.gitlab.sm1le.events.LiteEvent;
import org.bukkit.entity.EntityType;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class DragonFixPlugin extends JavaPlugin {

    @Override
    public void onEnable() {
        saveDefaultConfig();
        new LiteEvent()
                .event(EntitySpawnEvent.class, event -> {
                    if(getConfig().getList("worlds").contains(event.getEntity().getWorld().getName())) {
                        if(event.getEntityType() == EntityType.ENDER_DRAGON) event.setCancelled(true);
                    }
                }).register(this);
    }
}
